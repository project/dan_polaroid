Polaroid display mode for the theme "Day and Night" (DAN).

MAINTAINERS
-----------

Current maintainers:
 * Josef Friedrich (joseffriedrich) - https://drupal.org/user/580678
